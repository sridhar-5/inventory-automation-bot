export const data = [
  {
    name: "Indian Handicraft Jodhpur",
    Description: "visual art, a visual object or experience consciously created through an expression of skill or imagination.",
    imagePath: "https://api.twilio.com/2010-04-01/Accounts/ACbb82f6397d81636d4f02cf7dd1c081f5/Messages/MMd8edb196497c860cd470cc99c577a9bf/Media/ME84bdec4569e6d4fb886fcda0a96c7f0c",
    quantity: "20",
    price: 3000,
  },
   
  {
    name: "Artistic Pot",
    Description: "visual art, a visual object or experience consciously created through an expression of skill or imagination.",
    imagePath: "https://s3-external-1.amazonaws.com/media.twiliocdn.com/ACbb82f6397d81636d4f02cf7dd1c081f5/b288612d2a55ebc9af6dc1ebfc92c4c3",
    quantity: "20",
    price: 4000,
  },
  {
    name: "Artistic Lamp",
    Description: "visual art, a visual object or experience consciously created through an expression of skill or imagination.",
    imagePath: "https://api.twilio.com/2010-04-01/Accounts/ACbb82f6397d81636d4f02cf7dd1c081f5/Messages/MM5e324e5e0186c430e95dca68771616ae/Media/MEabde994938211bbb0d28d2df0a4b01df",
    quantity: "20",
    price: 5000,
  }
];
