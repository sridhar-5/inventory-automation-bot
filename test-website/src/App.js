import "./App.css";
import React, { useEffect, useState } from "react";
import Navbar from "./components/Navbar";
import Banner from "./components/banner.jsx";
import Product from "./components/Product.jsx";
import { Box, Grid } from "@mui/material";
// import { data } from "./utils/facultyDetails.js";
import axios from "axios";

function App() {
  const [data, setData] = useState(null);
  const [DataisLoaded, setDataLoaded] = useState(false);

  useEffect(() => {
    function fetchProducts() {
      axios
        .get("https://amazon-sambhav-website-server.herokuapp.com/getProducts")
        .then((response) => {
          console.log(response.data);
          setData(response.data);
          setDataLoaded(true);
        })
        .catch((error) => {
          console.log(error);
        });
    }
    setInterval(fetchProducts, 10000);
    return () => {
      clearInterval(10000);
    };
  }, []);

  if (!DataisLoaded)
    return (
      <>
        <h1> Pleses wait some time.... </h1>{" "}
      </>
    );

  const productList = data.map((product, index) => {
    return (
      <Grid item xs={3} key={index}>
        <Product
          imagePath={product.productImages[0]}
          name={product.productName}
          Description={product.productDescription}
          price={product.productPrice}
          quantity={product.productQuantity}
        />
      </Grid>
    );
  });

  return (
    <div className="App">
      <Navbar />
      <Banner />
      <br></br>
      <Box component="div" id="meet-the-team">
        <div className="team-title">Available Products</div>
        <Grid container spacing={2} className="details">
          {productList}
        </Grid>
      </Box>
    </div>
  );
}

export default App;
