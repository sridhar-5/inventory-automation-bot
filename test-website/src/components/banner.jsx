import React from "react";
import "./navbar.css";
import "bulma/css/bulma.min.css";

function Banner() {
  return (
    <section id="home" className="hero is-fullheight-with-navbar banner">
      <div className="hero-body">
        <div className="container">
          <p className="title">Kalakaar</p>
          <p className="subtitle taglead"><b>~ Your Inventory Management Bot ~</b></p>
          <p className="subtitle">
          <strong>Kalakaar </strong> is an automated inventory management system which utilizes Whatsapp bot technology for quick integration.
          Our aim is to maximize sales and profit of each and every retailer through the existing application and marketplaces through new architecture.
          </p>
        </div>
      </div>
    </section>
  );
}

export default Banner;

