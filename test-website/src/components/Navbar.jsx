import React from "react";
import Logo from "../assets/Kalakaar.png";
import "./navbar.css";
import "bulma/css/bulma.min.css";
import { Link } from "react-scroll";


function Navbar() {
  return (
    <nav className="navbar">
      <div className="navbar-brand logo-region">
        <a
          className="navbar-item"
          href="https://amrita.edu"
          target="_blank"
          rel="noreferrer"
        >
          <img src={Logo} className="logospace" alt="Amrita logo here" />
        </a>
      </div>

      <div className="navbar-menu">
        <div className="navbar-end">
          <Link
            activeClass="active"
            to="home"
            spy={true}
            smooth={true}
            offset={-70}
            duration={800}
            className="navbar-item"
          >Home</Link>
          <Link className="navbar-item" to="meet-the-team" spy={true}
            smooth={true}
            offset={-70}
            duration={800}>
            Products
          </Link>
          <Link className="navbar-item" to="footer" spy={true}
            smooth={true}
            offset={-70}
            duration={800}>
            Profile
          </Link>
        </div>
      </div>
    </nav>
  );
}

export default Navbar;